package com.springboot.camel.filesystem;

import java.util.Arrays;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileRouteBuilder extends RouteBuilder {
	
	@Override
	public void configure() throws Exception {
		// move data from one file another file
		System.out.println("started...");
		//moveAllFile();
		//moveSpecificFile("myFile");
		//moveSpecificFileWithBody("Java");
		//fileProcess();
		//multiFileProcessor();
		System.out.println("End...");

	}

	public void moveAllFile() {
		from("file:D:/Projects/Camel/a?noop=true").to("file:D:/Projects/Camel//b");
	}

	public void moveSpecificFile(String type) {
		from("file:D:/Projects/Camel/a?noop=true").filter(header(Exchange.FILE_NAME).startsWith(type))
				.to("file:D:/Projects/Camel/b");
	}

	public void moveSpecificFileWithBody(String content) {
		from("file:D:/Projects/Camel/a?noop=true").filter(body().startsWith(content))
				.to("file:D:/Projects/Camel/b");
	}

	public void fileProcess() {
		from("file:source?noop=true").process(p -> {
			String body = p.getIn().getBody(String.class);
			StringBuilder sb = new StringBuilder();
			Arrays.stream(body.split(" ")).forEach(s -> {
				sb.append(s + ",");
			});

			p.getIn().setBody(sb);
		}).to("file:destination?fileName=records.csv");
	}

	public void multiFileProcessor() {
		from("file:source?noop=true").unmarshal().csv().split(body().tokenize(",")).choice()
				.when(body().contains("Closed")).to("file:destination?fileName=close.csv")
				.when(body().contains("Pending")).to("file:destination?fileName=Pending.csv")
				.when(body().contains("Interest")).to("file:destination?fileName=Interest.csv");

	}

}
