package com.springboot.cassandra.entity;

import javax.persistence.Table;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@PrimaryKey
	private int id;
	private String name;
	private String address;
	private int age;

}