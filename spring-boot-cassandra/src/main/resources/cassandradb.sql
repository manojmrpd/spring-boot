create keyspace manoj with replication={'class':SimpleStrategy','replication_factor':1}

use manoj;

CREATE TABLE User(
   id int PRIMARY KEY,
   name text,
   address text,
   age int
   );