package com.springboot.mongodb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.mongodb.document.Users;
import com.springboot.mongodb.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
    private UserRepository userRepository;

    @GetMapping("/all")
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }
}