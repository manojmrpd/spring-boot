package com.springboot.mongodb.document;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Document
public class Users {

	@Id
    private Integer id;
    private String name;
    private String teamName;
    private Long salary;
    
}
