package com.springboot.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.springboot.mongodb.document.Users;

public interface UserRepository extends MongoRepository<Users, Integer> {
}
