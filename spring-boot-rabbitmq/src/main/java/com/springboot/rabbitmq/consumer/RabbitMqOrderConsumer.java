package com.springboot.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.springboot.rabbitmq.config.RabbitMQConfig;
import com.springboot.rabbitmq.dto.OrderStatus;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RabbitMqOrderConsumer {
	
	@RabbitListener(queues = RabbitMQConfig.QUEUE)
    public void onMessage(OrderStatus orderStatus) {
        log.info("Message Received from Queue {} and Message is {} "+RabbitMQConfig.QUEUE, orderStatus.toString());
    }

}
