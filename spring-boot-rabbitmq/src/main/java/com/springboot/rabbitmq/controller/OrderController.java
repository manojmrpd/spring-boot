package com.springboot.rabbitmq.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.rabbitmq.dto.Order;
import com.springboot.rabbitmq.dto.OrderStatus;
import com.springboot.rabbitmq.producer.RabbitMqOrderProducer;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

	@Autowired
	RabbitMqOrderProducer orderProducer;

	@PostMapping("/{restaurantName}")
	public ResponseEntity<Order> bookOrder(@RequestBody Order order, @PathVariable String restaurantName) {
		order.setOrderId(UUID.randomUUID().toString());
		OrderStatus orderStatus = new OrderStatus(order, "PROCESSING", "order placed succesfully in " + restaurantName);
		orderProducer.publishMessage(orderStatus);
		return new ResponseEntity<Order>(order, HttpStatus.CREATED);
	}

}
