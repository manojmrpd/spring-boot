package com.springboot.rabbitmq.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springboot.rabbitmq.config.RabbitMQConfig;
import com.springboot.rabbitmq.dto.OrderStatus;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RabbitMqOrderProducer {
	
	@Autowired
    private RabbitTemplate rabbitTemplate;

	public void publishMessage(OrderStatus orderStatus) {
		
		rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE, RabbitMQConfig.ROUTING_KEY, orderStatus);
        log.info("Message succesfully sent in to EXCHANGE {}, with OrderStatus {}"+RabbitMQConfig.EXCHANGE , orderStatus.toString());
		
	}

}
