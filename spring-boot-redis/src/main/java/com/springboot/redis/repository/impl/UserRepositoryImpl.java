package com.springboot.redis.repository.impl;

import java.util.Map;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.springboot.redis.model.User;
import com.springboot.redis.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private RedisTemplate<String, User> redisTemplate;

    private HashOperations hashOperations;


    public UserRepositoryImpl(RedisTemplate<String, User> redisTemplate) {
        this.redisTemplate = redisTemplate;
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void save(User user) {
        hashOperations.put("user", user.getId(), user);
    }

    @Override
    public Map<String, User> findAll() {
        return hashOperations.entries("user");
    }

    @Override
    public User findById(String id) {
        return (User)hashOperations.get("user", id);
    }

    @Override
    public void update(User user) {
    	hashOperations.put("user", user.getId(), user);
    }

    @Override
    public void delete(String id) {

        hashOperations.delete("USER", id);
    }
}
